CREATE TABLE enm_permission
(
    id BIGINT(20) UNSIGNED PRIMARY KEY NOT NULL,
    name VARCHAR(191) NOT NULL
);


CREATE UNIQUE INDEX enm_permission_name_uindex ON enm_permission (name);

CREATE TABLE t_role_permission (
  role_id INT(10) UNSIGNED NOT NULL,
  permission_id BIGINT(20) UNSIGNED NOT NULL,
  CONSTRAINT `t_role_permission_role_id` FOREIGN KEY (`role_id`) REFERENCES enm_role (id) ON DELETE CASCADE,
  CONSTRAINT `t_role_permission_permission_id` FOREIGN KEY (`permission_id`) REFERENCES enm_permission (id) ON DELETE CASCADE
);
