<?php

namespace Insolutions\Auth;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    //
     protected $table = "enm_permission";

     public static function findByName($name) {
        return self::where('name', $name)->firstOrFail();
     }

    public function roles() {
        return $this->belongsToMany(Role::class, 't_role_permission');
    }
}
