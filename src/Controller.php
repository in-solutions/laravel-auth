<?php

namespace Insolutions\Auth;
 
use App\User;
use Illuminate\Http\Request;

use Auth, Hash;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Controller extends \App\Http\Controllers\Controller
{
    
	public function getMe(Request $request) {
        $user = Auth::user();

        $roles = [];
        $permissions = [];

        if ($user) {
            $roles = UserRole::with(['role'])->where('user_id', $user->id)->get()->map(
                function ($ur) {
                    return $ur->role->name;
                }
            );

            $permissions = Permission::whereHas('roles', function ($q) use ($user) {
               $q->whereHas('userRoles', function ($q) use ($user) {
                   $q->where('user_id', $user->id);
               });
            })->get()->pluck('name')->unique();
        }

        $result = [
            'user' => $user ? $user->makeHidden('pin_code') : null,
            'roles' => $roles,
            'permissions' => $permissions
        ];

        return response()->json($result);
    }

    public function authenticate(Request $request, $role = null)
    {
        // TODO: check if request is coming from admin,
        // if is in the roles ... success
        // if not 401 nemas rolu
        $isAuthenticated = false;
        if ($request->login) {
            if ($request->password) {
                // new behaviour, check login against both login and email
                $isAuthenticated = Auth::attempt(['login' => $request->login, 'password' => $request->password], true);

                if (!$isAuthenticated) {
                    $isAuthenticated = Auth::attempt(['email' => $request->login, 'password' => $request->password], true);
                }
            }

            if (!$isAuthenticated) {
                $user = User::whereNotNull('pin_code')->where(['pin_code' => md5($request->login)])->first();
                if ($user) {
                    $isAuthenticated = true;
                    Auth::login($user);
                }
            }
        } else {
            // legacy behaviour, only check email
            $isAuthenticated = Auth::attempt(['email' => $request->email, 'password' => $request->password], true);
        }

        if ($isAuthenticated) {
            if ($role !== null) {                
                $roleModel = Role::getByName($role);                   
                if (!UserRole::hasUserRole(Auth::user(), $roleModel)) {
                    // user can be logged but has not specified role
                    return response("Missing role", 401);
                }
            }

            // Authentication passed...
            $session = Session::getActive();
            $session->assignUser(Auth::user());
            $session->loginUser();
            return response()->json(['token' => $session->id], 200);
        } else {
        	return response()->json("Wrong credentials!", 401);
        }
    }

    public function logout() 
    {
    	Auth::logout();
    	Session::getActive()->unassignUser();
    	return redirect('');
    }
    
    public function requestPasswordReset(Request $r) {
        $passwordReset = new PasswordReset($r->all());
        $passwordReset->token = Str::random(60);

        $passwordReset->save();

        event(new EventPasswordResetRequested($passwordReset));
    }
    
    public function validateResetRequest(Request $r, $token) {
        $passwordReset = PasswordReset::where('token', $token)->firstOrFail();

        return response()->json($passwordReset);

    }
    
    public function resetPassword(Request $r, $token) {
        DB::beginTransaction();

        $passwordReset = PasswordReset::where('token', $token)->firstOrFail();

        if ($r->password) {
            if (!$r->password2 || $r->password != $r->password2) {
                return response()->json([
                    'message' => 'Passwords do not match'
                ], Response::HTTP_BAD_REQUEST);
            }
        } else {
            return response()->json([
                    'message' => 'New password not provided'
                ], Response::HTTP_BAD_REQUEST);
        }

        $user = User::where('email', $passwordReset->email)->firstOrFail();
        $user->password = Hash::make($r->password);
        $user->save();

        $session = Session::getActive();
        $session->assignUser($user);
        $session->loginUser();

        event(new EventPasswordResetSuccess($user));

        // deactivate all password reset requests for this email
        PasswordReset::where('email', $passwordReset->email)->delete();

        DB::commit();

        return response()->json(['token' => $session->id], Response::HTTP_OK);
    }

}