<?php

namespace Insolutions\Auth;

use Illuminate\Queue\SerializesModels;

class EventPasswordResetRequested
{
    use SerializesModels;

    public $passwordReset;

    /**
     * Create a new event instance.
     *
     * @param  PasswordReset $passwordReset
     * @return void
     */
    public function __construct(PasswordReset $passwordReset)
    {
        $this->passwordReset = $passwordReset;
    }
}