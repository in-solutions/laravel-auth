<?php

Route::group([
    'prefix' => 'auth',
    'middleware' => [
        \Insolutions\Auth\Middleware::class
    ]
], function () {
	
	Route::post('login', 'Insolutions\Auth\Controller@authenticate');
	Route::match(['post','get'], 'logout', 'Insolutions\Auth\Controller@logout');

	Route::get('user/me', 'Insolutions\Auth\Controller@getMe');
	Route::post('password-reset', 'Insolutions\Auth\Controller@requestPasswordReset');
	Route::get('password-reset/{token}', 'Insolutions\Auth\Controller@validateResetRequest');
	Route::post('password-reset/{token}', 'Insolutions\Auth\Controller@resetPassword');

});