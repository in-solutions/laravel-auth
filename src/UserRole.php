<?php

namespace Insolutions\Auth;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;

class UserRole extends Model
{
    use SoftDeletes;

    //
    protected $table = 't_user_role';

    /**
     *  Check if user is in the table and has role
     */
    public static function hasUserRole (User $user, Role $role) {
        $userRole = self::where('user_id', $user->id)->where('role_id', $role->id)->first();

        return ($userRole != null); // true if role was found, false if userRole result is null
    }

    public function role() {
        return $this->belongsTo(Role::class);
    }
}
