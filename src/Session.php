<?php

namespace Insolutions\Auth;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Auth, App\User;

class Session extends Model
{
    use SoftDeletes;

    protected $table = 't_session';

    public $fillable = [
        'ip_address','user_agent','payload',
    ];

    public $incrementing = false;

    // singleton implementation for active session
    private static $instance = null;

    public static function getActive() {
        return self::$instance;
    }

    public static function setActive(Session $session) {
        self::$instance = $session;
    }

    protected static function boot()
    {
        parent::boot();

        /**
         * Attach to the 'creating' Model Event to provide a UUID
         * for the `id` field (provided by $model->getKeyName())
         */
        static::creating(function ($model) {
            $model->id = uniqid();
        });
    }

    public static function start(User $user = null) {
    	$sess = new self();
    	if ($user) {
            $sess->user_id = $user->id;
        }
    	$sess->save();
    	return $sess;
    }

    public function put($key, $value) {

    }

    public function getPayloadAttribute($payload) {
        return json_decode($payload ?: "{}");
    }

    public function setPayloadAttribute($data) {
        $this->attributes['payload'] = json_encode($data);
    }

    public function assignUser(User $user) {
        $this->user()->associate($user);
        return $this->save();
    }

    public function unassignUser() {
        if ($this->user_id) {
            $this->addUserToUserHistory($this->user);

            $this->user()->dissociate();
        }        
        return $this->save();
    }

    private function addUserToUserHistory(User $user) {
        // save user_id into session history            
        $payload = $this->payload;
        if (!isset($payload->_user_history)) {
            $payload->_user_history = array();
        } 
        if (!in_array($this->user_id, $payload->_user_history)) {
            $payload->_user_history[] = $this->user_id;
        }
        $this->payload = $payload;            
    }

    public function loginUser() {    	
        return $this->user ? Auth::loginUsingId($this->user->id) : null;
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
