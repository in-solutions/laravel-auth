<?php
namespace Insolutions\Auth;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PasswordReset extends Model {
    use SoftDeletes;
    
    protected $table = 't_password_reset';

    public $fillable = ['email'];
}