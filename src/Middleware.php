<?php

namespace Insolutions\Auth;

use Closure;
use Illuminate\Support\Facades\Auth;

class Middleware
{
    const AUTH_QUERY_PARAM_NAME = '_token';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // load token as Bearer token, or (deprecated) from X-Auth-Token header
        $token = $request->bearerToken() ?: $request->header('X-Auth-Token');
        if ($request->query(self::AUTH_QUERY_PARAM_NAME)) {
          $token = $request->query(self::AUTH_QUERY_PARAM_NAME);
        }
        // token present - try to load and login user
        if ($token) {
          // load existing session
          $session = Session::find($token);
        }

        if (isset($session)) {
          $session->loginUser();
          $session->touch();
        } else {
          // start new session
          $session = Session::start();
        }

        Session::setActive($session);
        // TODO: register active session

        /*
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson() || $this->isJsonMimeType($request)) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }
        */

        $response = $next($request);

        $response->headers->set('X-Auth-Token', $session->id);

        return $response;
    }

    private function isJsonMimeType($request) {
       /* 2. Then check the Content-Type */
       $content_type = $request->header('Content-Type');
       
       $allowable_types = array(
               'application/json', 
               'application/javascript',
       );

       if (in_array(
               strtolower($content_type),
               $allowable_types)) 
       {
           return true;
       }
    }
}
