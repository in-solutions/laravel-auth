<?php

namespace Insolutions\Auth;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {        
        
        $this->publishes([
            __DIR__.'/../db' => base_path('database/sql/insolutions/auth'),
            __DIR__.'/../app/Listeners' => app_path('Listeners'),
//            __DIR__.'/../views' => base_path('resources/views/insolutions/auth'),
//            __DIR__.'/assets' => public_path('insolutions/auth'),
            __DIR__.'/../config' => base_path('config'),
        ]);

        if (config('insolutions-auth.route_group')) {
            $groups = (array) config('insolutions-auth.route_group', 'api');
            foreach ($groups as $group) {
                $this->app->make('Illuminate\Routing\Router')->prependMiddlewareToGroup($group, 'Insolutions\Auth\Middleware');
            }
        }

        $this->app['router']->aliasMiddleware(config('insolutions-auth.middleware_alias', 'auth'), MiddlewareOnlyAuth::class);
        //$this->app->make('Illuminate\Contracts\Http\Kernel')->prependMiddleware('\Barryvdh\Cors\HandleCors');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make('Insolutions\Auth\Controller');
    }
}
