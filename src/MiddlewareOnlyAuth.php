<?php

namespace Insolutions\Auth;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class MiddlewareOnlyAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$permissions
     * @return mixed
     */
    public function handle($request, Closure $next, ...$permissions)
    {
        if (!Auth::user()) {
	    	return response('Unauthenticated! (no logged user)', 401);
	    }

        if ($permissions) {
            $user = Auth::user();

            $ownedPermissions = Permission::whereIn('name', $permissions)
            ->whereHas('roles', function ($q) use ($user) {
               $q->whereHas('userRoles', function ($q) use ($user) {
                   $q->where('user_id', $user->id);
               });
            })->get()->pluck('name')->unique();

            $allowed = $ownedPermissions->count() > 0;
            if (!$allowed) {
                return response('Unauthorized! (missing permission)', Response::HTTP_UNAUTHORIZED);
            }

        }

	    return $next($request);
    }
}
