<?php

namespace Insolutions\Auth;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
     protected $table = "enm_role"; 

     public static function findByName($name) {
        return self::where('name', $name)->firstOrFail();
     }

    public function permissions() {
        return $this->belongsToMany(Permission::class, 't_role_permission');
    }

    public function userRoles() {
        return $this->hasMany(UserRole::class, 'role_id');
    }
}
