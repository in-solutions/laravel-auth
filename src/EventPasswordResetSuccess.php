<?php

namespace Insolutions\Auth;

use App\User;
use Illuminate\Queue\SerializesModels;

class EventPasswordResetSuccess
{
    use SerializesModels;

    public $user;

    /**
     * Create a new event instance.
     *
     * @param  PasswordReset $passwordReset
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}