<?php
namespace App\Listeners;

class AuthSubscriber
{
    public function onPasswordResetRequested($event) {
        // newly created PasswordReset: $event->passwordReset
    }

    public function onPasswordResetSuccess($event) {
        // user who's password was reset: $event->user
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Insolutions\Auth\EventPasswordResetRequested',
            'App\Listeners\AuthSubscriber@onPasswordResetRequested'
        );

        $events->listen(
            'Insolutions\Auth\EventPasswordResetSuccess',
            'App\Listeners\AuthSubscriber@onPasswordResetSuccess'
        );

    }

}